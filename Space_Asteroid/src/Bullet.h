#pragma once
#include "ofMain.h"
#include "GameObject.h"

/**
 * Bullet.h
 *
 * Header file for Bullet object inherits from GameObject class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

class Bullet : public GameObject
{
public:
	//Methods
	Bullet(float x, float y);
	~Bullet();
	void update();
	void draw();
	
};

