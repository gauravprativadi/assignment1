#include "GameObject.h"
/**
 * GameObject.cpp
 *
 * Class file for Game object
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

GameObject::GameObject() {}

GameObject::~GameObject() {}


void GameObject::update() {}

void GameObject::draw() {}

/* 
	Method: checkCollisions(GameObject *obj)
	
	A method to check collisions between objects in the game. 
	Returns true if collision occured between this object and the object given as a parameter.
	Returns false otherwise
*/

bool GameObject::checkCollisions(GameObject *obj)
{
	// Checking the distance between the objects
	float dist = ofDist(position.x, position.y, obj->position.x, obj->position.y);
	float minDist = obj->size;

	// If distance calculated is less than minDist, returns true for collision else returns false
	if (dist <= minDist) {
		return true;
	}
	return false;
}
