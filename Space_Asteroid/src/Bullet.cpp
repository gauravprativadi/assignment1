#include "Bullet.h"

/**
 * Bullet.cpp
 *
 * Class file for Bullet object which inherits GameObject
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

/*
	Constructor Method

	Initializes size, position and speed of each bullet when an instance of the class is created
*/
Bullet::Bullet(float x, float y)
{
	size = 4;
	position.x = x;
	position.y = y;
	speed = 9;
}

// Deconstructor
Bullet::~Bullet()
{
}

/*
	Method: update()

	Updates the position of the bullet every frame 
*/
void Bullet::update()
{
	position.y -= speed;
}

/*
	Method: draw()

	Draws the bullet instance on the screen.
*/
void Bullet::draw()
{
	//ofPushStyle();
	//ofPushMatrix();

	//Setting the color to yellow
	ofColor(255, 255, 0);

	//Drawing a circle each frame
	ofDrawCircle(position.x, position.y, size);
	
	//ofPopMatrix();
	//ofPopStyle();
}
