#include "Asteroid.h"

/**
 * Asteroid.cpp
 *
 * Header file for Bullet object inherits from GameObject class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

/*
	Constructor method

	Initializes the size, position and speed of the asteroids when an instance of the class is created

*/
Asteroid::Asteroid()
{
	size = ofRandom(30, 50);
	position.x = ofRandom(100, 900);
	position.y = 0;
	speed = 2;
}

/*
	Deconstructor method 

*/
Asteroid::~Asteroid()
{
}

/*
	Method: update()

	Updates the position of the asteroid each frame
*/
void Asteroid::update()
{
	position.y += speed;
}

/*
	Method: draw()

	Draws the asteroid instance on the screen.

*/
void Asteroid::draw()
{

	//Sets color to gray
	ofSetColor(100,100,100);
	
	// Draws circle on the screen at the position each frame
	ofDrawCircle(this->position.x, this->position.y, this->size);

}
