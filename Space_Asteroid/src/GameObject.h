#pragma once
#include "ofMain.h"

/**
 * GameObject.h
 *
 * Header file for Game object
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

class GameObject
{
public:
	//Methods
	GameObject();
	~GameObject();

	void update();
	void draw();
	bool checkCollisions(GameObject *obj);

	//Variables
	int size;			// Size of the object
	int speed;			// Speed of the object
	ofPoint position;	// Position of the gameobject

};

