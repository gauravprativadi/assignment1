#pragma once
#include "ofMain.h"
#include "GameObject.h"

/**
 * Asteroid.h
 *
 * Header file for Asteroid object that inherits from GameObject class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

class Asteroid : public GameObject {

public:
	//Methods
	Asteroid();
	~Asteroid();
	
	void update();
	void draw();


};