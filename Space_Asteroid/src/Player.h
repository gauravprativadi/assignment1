#pragma once
#include "ofMain.h"
#include "GameObject.h"
#include "Bullet.h"

/**
 * Player.h
 *
 * Header file for player object that inherits from GameObject class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

class Player : public GameObject {

public:
	//Methods
	Player();
	~Player();

	void reinitialize();
	void updatePlayer();
	void drawPlayer();
	void createBullet();
	void drawBullets();
	void updateBullets();

	// Variables

	float vx, vy; // Speed for x axis and y axis 
	bool leftPressed, rightPressed, isFiring = false; 
	int bulletCount; // Bullet count

	//Vector to hold the bullet objects
	vector<Bullet*> bullets;
};