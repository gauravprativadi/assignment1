#include "ofApp.h"

/**
 * OfApp.cpp
 *
 * Class file for the App class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

/*
	Method: setup()

	First method that is called when the class is created
*/
void ofApp::setup(){
	ofEnableAntiAliasing();

	player = new Player();
}


/*
	Method: update()

	Method is called every frame. 
	Updates all the variables and calls the update functions of other game objects
*/
void ofApp::update(){
	
	//Updating the asteroid instances
	for (unsigned int i = 0; i < asteroids.size(); ++i) {
		Asteroid* currentAsteroid = asteroids[i];
		currentAsteroid->update();
		// Delete asteroids if they go out of the screen
		if (currentAsteroid->position.y > ofGetHeight() + (int)currentAsteroid->size) {
			delete currentAsteroid;
			asteroids.erase(asteroids.begin() + i);
			i--;
			points--;
		}

	}

	// Call the update function of the player
	player->updatePlayer();

	//Checks for collisions
	checkCollisions();

}

/*
	Method: draw()

	Method is called every frame.

	Calls the draw functions of all the game objects and prints the score on the screen
*/
void ofApp::draw(){
	ofClear(0);
	ofSetColor(255, 255, 255);
	//Print the score on the screen
	ofDrawBitmapString("Score: " + ofToString(points), ofGetWidth() - 100, 50);
	//Calls the draw function of the player object
	player->drawPlayer();
	//Calls the draw function of the asteroid object
	drawAsteroid();
}

/*
	Method: drawAsteroid
*/
void ofApp :: drawAsteroid() {

	// Creating asteroids if the count decreases below the frequency
	if (ofGetFrameNum() % asteroidFrequency == 0) {
		Asteroid* asteroid = new Asteroid();
		asteroids.push_back(asteroid);
	}

	// calls the draw function of each asteroid instance
	for (unsigned int i = 0; i < asteroids.size(); ++i) {
		asteroids[i]->draw();
	}
}

/*
	Method: checkCollisions()

	Checks for collisions between player / asteroid and asteroid / bullet objects
*/
void ofApp::checkCollisions()
{
	// Collision check for player/asteroid
	for (unsigned int i = 0; i < asteroids.size(); ++i) {
		if (player->checkCollisions(asteroids[i])) {
			//Calls the reinitialize function of the player object
			player->reinitialize();
			//Resets the asteroids
			asteroids.clear();
			//Reset the score
			points = 0;
			break;
		}
	}

	// Collision check for bullets / asteroids game objects
	if (player->bullets.size() > 0) {
		for (unsigned int i = 0; i < player->bullets.size(); i++) {
			for (unsigned int j = 0; j < asteroids.size(); j++) {
				if (player->bullets.size() > 0) {
					//Checks for the collision
					if (player->bullets[i]->checkCollisions(asteroids[j])) {
						//Delete the bullet instance and the asteroid instance
						delete player->bullets[i];
						delete asteroids[j];
						// Increase the score
						points ++; 
						// Remove the instances from their respective vectors
						player->bullets.erase(player->bullets.begin() + i);
						asteroids.erase(asteroids.begin() + j);
						i--;
						j--;
					}
				}
			}
		}
	}
}


/*
	Method: keyPressed(int key)

	Updating the boolean variables as per the key press
*/
void ofApp::keyPressed(int key){
	if (key == OF_KEY_LEFT) {
		player->leftPressed = true;
	}
	else if (key == OF_KEY_RIGHT) {
		player->rightPressed = true;
	}
	else if (key == ' ') {
		player->isFiring = true;
	}
}

/*
	Method: keyReleased(int key)

	Updating the boolean variables as per the key release
*/
void ofApp::keyReleased(int key){

	if (key == OF_KEY_LEFT) {
		player->leftPressed = false;
	}
	else if (key == OF_KEY_RIGHT) {
		player->rightPressed = false;
	}
	else if (key == ' ') {
		player->isFiring = false;
		player->bulletCount = 0;
	}
}
