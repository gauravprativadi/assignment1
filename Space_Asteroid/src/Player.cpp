#include "Player.h"

/**
 * Player.cpp
 *
 * Class file for player object that inherits from GameObject class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

/*
	Constructor method

	Initializes the variables
*/
Player::Player()
{
	position.x = ofGetWidth() / 2;
	position.y = ofGetHeight() - 50;
	vy = 0;
	vx = 0;
	speed = 6;
	bulletCount = 0;
	leftPressed = rightPressed = isFiring = false;
}

/*
	Deconstructor

*/
Player::~Player()
{
}

/*
	Method: reinitialize()

	Resets the values of the variables 
*/
void Player::reinitialize() {
	position.x = (ofGetWidth() ) / 2;
	position.y = ofGetHeight() - 50;
	vy = 0;
	vx = 0;
	speed = 6;
	bulletCount = 0;
	leftPressed = rightPressed = isFiring = false;

}

/*
	Method: updatePlayer()

	Updates the position, speed of the player every frame
*/
void Player::updatePlayer() {

	//Updating the velocity accordingly if left or right key is pressed
	
	if (leftPressed == true) {
		vx = -speed;
	}
	else if (rightPressed == true) {
		vx = speed;
	}
	else {
		vx = 0;
	}

	// Updating the position as per the velocity 
	position.x += vx;

	// Enabling warping on the sides 
	if (position.x + 10 < 0)
		position.x = ofGetWidth() + 9;

	if (position.x - 10 > ofGetWidth())
		position.x = -9;

	// Check if the player is shooting
	if (isFiring) {
		createBullet(); // Creates a bullet instance
	}

	//Updating bullets
	updateBullets();

}

/*
	Method drawPlayer()

	Draws the instance of the player on the screen
*/
void Player::drawPlayer()
{

	//ofPushStyle();
	//ofPushMatrix();

	//Drawing the inside red filled triangle
	ofSetColor(255, 0, 0);
	ofDrawTriangle(position.x, position.y - 17.32, position.x - 10, position.y, position.x + 10, position.y);

	//Drawing the outher white triangle
	ofSetColor(255);
	ofNoFill();
	ofDrawTriangle(position.x, position.y - 17.32, position.x - 10, position.y, position.x + 10, position.y);
	ofFill();
	

	//ofPopMatrix();
	//ofPopStyle();

	//Draw bullets
	drawBullets();
	
}

/*
	Method: createBullet()

	Creates an instance of the bullet object, adds it to the vector and increases the bullet count
*/
void Player::createBullet() {
	if (bulletCount < 1) {
		Bullet *bullet = new Bullet(position.x, position.y - 17.32);
		bullets.push_back(bullet);
		++bulletCount;
	}
}

/*
	Method: drawBullets()

	Calls the draw function of the Bullet class for every instance of bullets created
*/
void Player::drawBullets() {
	if (bullets.size() > 0) {
		for (unsigned int i = 0; i < bullets.size(); ++i) {
			bullets[i]->draw();
		}
	}
}

/*
	Method: updateBullets()

	Calls the update function of the Bullet class for every instance of bullets created
*/

void Player::updateBullets()
{
	if (bullets.size() > 0) {
		for (unsigned int i = 0; i < bullets.size(); ++i) {
			// Call the update function
			bullets[i]->update();

			// Delete the bullet if it is outside the screen
			if (bullets[i]->position.y < 0) {
				delete bullets[i];
				//Remove the instance from the vector
				bullets.erase(bullets.begin() + i);
			}
		}
	}
}

