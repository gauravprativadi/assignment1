#pragma once

/**
 * OfApp.h
 *
 * Header file for the App class
 *
 * Gaurav Prativadi
 * EDPX - 3330
 *
 */

#include "ofMain.h"
#include "Asteroid.h"
#include "Player.h"

class ofApp : public ofBaseApp{

	public:

		//Methods
		void setup();
		void update();
		void draw();
		void drawAsteroid();
		void checkCollisions();
		void keyPressed(int key);
		void keyReleased(int key);
		
		// Variables

		// Vector to hold the asteroid instances created
		vector<Asteroid*> asteroids;
		// Variable for asteroid Frequency. (Lower = More Asteroids)
		int asteroidFrequency = 60;  
		//Player object
		Player *player;
		//Score 
		int points;
};
